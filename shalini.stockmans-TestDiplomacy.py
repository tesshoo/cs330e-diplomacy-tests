#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve, find_unique_max

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Houston hold\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i,  "A")
        self.assertEqual(j, "Houston")
        self.assertEqual(k, "hold")
        self.assertEqual(l, None)

    def test_read_2(self):
        s = "B Raleigh Support A\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i,  "B")
        self.assertEqual(j, "Raleigh")
        self.assertEqual(k, "Support")
        self.assertEqual(l, "A")

    def test_read_3(self):
        s = "C Miami Move Charlotte\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i,  "C")
        self.assertEqual(j, "Miami")
        self.assertEqual(k, "Move")
        self.assertEqual(l, "Charlotte")


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {'B':'[dead]','A':'Houston'})
        self.assertEqual(w.getvalue(), "A Houston\nB [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {'A':'[dead]','B':'Houston', 'C':'[dead]', 'D':'Austin'})
        self.assertEqual(w.getvalue(), "A [dead]\nB Houston\nC [dead]\nD Austin\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {'B':'[dead]', 'A':'Houston', 'E':'[dead]', 'C':'Denver', 'D':'[dead]'})
        self.assertEqual(w.getvalue(), "A Houston\nB [dead]\nC Denver\nD [dead]\nE [dead]\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_4(self):
        r = StringIO("A Denver Hold\nB Seattle Support A\nC Portland Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Denver\nB Seattle\nC Portland\n")

    def test_solve_5(self):
        r = StringIO("A Denver Hold\nB Seattle Support A\nC Portland Support A\nD Houston Move Portland\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Denver\nB Seattle\nC [dead]\nD [dead]\n")


# -----
# find_unique_max
# -----

    def test_find_unique_max_1(self):
        arr = [0,1,2,1,1,0]
        val, idx = find_unique_max(arr)
        self.assertEqual(val, 2)
        self.assertEqual(idx, 2)

    def test_find_unique_max_2(self):
        arr = [0,0,1,1]
        val, idx = find_unique_max(arr)
        self.assertEqual(val, None)
        self.assertEqual(idx, -1)
    
    def test_find_unique_max_3(self):
        arr = [0]
        val, idx = find_unique_max(arr)
        self.assertEqual(val, 0)
        self.assertEqual(idx, 0)

    def test_find_unique_max_4(self):
        arr = [1,1,1,1,1,1]
        val, idx = find_unique_max(arr)
        self.assertEqual(val, None)
        self.assertEqual(idx, -1)

# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()

