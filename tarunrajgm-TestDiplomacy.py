#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve


# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        readout = diplomacy_read(s)
        read_str = " ".join(readout)
        self.assertEqual(read_str, "A Madrid Hold")

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        readout = diplomacy_read(s)
        read_str = " ".join(readout)
        self.assertEqual(read_str, "B Barcelona Move Madrid")

    def test_read_3(self):
        s = "C London Support B\n"
        readout = diplomacy_read(s)
        read_str = " ".join(readout)
        self.assertEqual(read_str, "C London Support B")

    def test_read_4(self):
        s = "C Austin Move Dallas\n"
        readout = diplomacy_read(s)
        read_str = " ".join(readout)
        self.assertEqual(read_str, "C Austin Move Dallas")

    def test_read_5(self):
        s = "Z Munich Hold\n"
        readout = diplomacy_read(s)
        read_str = " ".join(readout)
        self.assertEqual(read_str, "Z Munich Hold")

    # ----
    # print
    # ----

    def test_print_1(self):
        w = StringIO()
        s = [('A', '[dead]'), ('B', '[dead]')]
        diplomacy_print(w, s)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_print_2(self):
        w = StringIO()
        s = [('A', 'Paris'), ('B', '[dead]')]
        diplomacy_print(w, s)
        self.assertEqual(w.getvalue(), "A Paris\nB [dead]\n")

    def test_print_3(self):
        w = StringIO()
        s = [('A', 'Paris'), ('B', '[dead]'), ('C', 'Austin')]
        diplomacy_print(w, s)
        self.assertEqual(w.getvalue(), "A Paris\nB [dead]\nC Austin\n")

    def test_print_4(self):
        w = StringIO()
        s = [('A', '[dead]')]
        diplomacy_print(w, s)
        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print_5(self):
        w = StringIO()
        s = [('A', '[dead]'), ('B', '[dead]'), ('C', '[dead]'), ('D', '[dead]')]
        diplomacy_print(w, s)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    # ----
    # eval
    # ----

    def test_eval_1(self):
        scenario = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']]
        res = diplomacy_eval(scenario)
        self.assertEqual(res, [('A', '[dead]'), ('B', 'Madrid'), ('C', 'London')])

    def test_eval_2(self):
        scenario = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid']]
        res = diplomacy_eval(scenario)
        self.assertEqual(res, [('A', '[dead]'), ('B', '[dead]')])

    def test_eval_3(self):
        scenario = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B'],
                    ['D', 'Austin', 'Move', 'London']]
        res = diplomacy_eval(scenario)
        self.assertEqual(res, [('A', '[dead]'), ('B', '[dead]'), ('C', '[dead]'), ('D', '[dead]')])

    def test_eval_4(self):
        scenario = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid']]
        res = diplomacy_eval(scenario)
        self.assertEqual(res, [('A', '[dead]'), ('B', '[dead]'), ('C', '[dead]')])

    def test_eval_5(self):
        scenario = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'],
                    ['D', 'Paris', 'Support', 'B']]
        res = diplomacy_eval(scenario)
        self.assertEqual(res, [('A', '[dead]'), ('B', 'Madrid'), ('C', '[dead]'), ('D', 'Paris')])

    def test_eval_6(self):
        scenario = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'],
                    ['D', 'Paris', 'Support', 'B'], ['E', 'Austin', 'Support', 'A']]
        res = diplomacy_eval(scenario)
        self.assertEqual(res, [('A', '[dead]'), ('B', '[dead]'), ('C', '[dead]'), ('D', 'Paris'), ('E', 'Austin')])

    def test_eval_7(self):
        scenario = [['A', 'Madrid', 'Hold']]
        res = diplomacy_eval(scenario)
        self.assertEqual(res, [('A', 'Madrid')])

    # ----
    # solve
    # ----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        res = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Paris Support B\nB London Move Munich\nC Munich Hold\nD Austin Support B\n")
        w = StringIO()
        res = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Paris\nB Munich\nC [dead]\nD Austin\n")

    def test_solve_3(self):
        r = StringIO("A Paris Support B\nB Munich Move London\nC Munich Hold\nD Austin Support B\n")
        w = StringIO()
        res = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Paris\nB London\nC Munich\nD Austin\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Brussels Support C\nC London Hold\nD Paris Move London\nE Berlin Support A")
        w = StringIO()
        res = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Brussels\nC London\nD [dead]\nE Berlin\n")

    def test_solve_5(self):
        r = StringIO("A Paris Support B\nB London Support A\nC Austin Hold")
        w = StringIO()
        res = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Paris\nB London\nC Austin\n")

    def test_solve_6(self):
        r = StringIO("D Austin Support B\nA Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n"
                     "E Paris Support B\nF Berlin Support B\nG Manchester Support A\n")
        w = StringIO()
        res = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Austin\nE Paris\nF Berlin\nG Manchester\n")

    def test_solve_7(self):
        r = StringIO("A Madrid Hold\nB London Support A\nC Paris Move Madrid\nD Berlin Support A\n"
                     "E Munich Support C\nF Rome Move Madrid\nG Venice Support F\n")
        w = StringIO()
        res = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB London\nC [dead]\nD Berlin\nE Munich\nF [dead]\nG Venice\n")

    def test_solve_8(self):
        r = StringIO("A Madrid Move London\nB Barcelona Move Madrid\nC London Move Barcelona\n")
        w = StringIO()
        res = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A London\nB Madrid\nC Barcelona\n")

    def test_solve_9(self):
        r = StringIO("A Madrid Hold \nB Barcelona Move Madrid\nC London Support B\nD Rome Move Madrid\n"
                     "E Munich Support D\nF Austin Support A\n")
        w = StringIO()
        res = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD [dead]\nE Munich\nF Austin\n")

    def test_solve_10(self):
        r = StringIO("A Madrid Hold\nB London Support A\nC Barcelona Support B\nD Rome Move Madrid\n"
                     "E Venice Move Madrid\n")
        w = StringIO()
        res = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB London\nC Barcelona\nD [dead]\nE [dead]\n")

    def test_solve_11(self):
        r = StringIO("A Madrid Hold\nB London Support A\nC Barcelona Move Madrid\nD Rome Support C\n"
                     "E Venice Move Madrid\nF Berlin Support E\n")
        w = StringIO()
        res = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB London\nC [dead]\nD Rome\nE [dead]\nF Berlin\n")

    def test_solve_12(self):
        r = StringIO("")
        w = StringIO()
        res = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")


# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()
